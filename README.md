# ![img](https://framagit.org/flabarrere/flopedt_android/raw/master/asset/favicon/ic_flopedt_round.png) FlopEDT-Android

Une application simple et [libre][libre] pour acceder à son [FlOpEDT][flopedt].

## Atouts

* Simple d'utilisation
* Simple a configurer
* Léger
* Personnalisé
* Adaptable

## Pourquoi FlOpEDT-android?

Le but de FlOpEDT-android est de complementer l'outil [FlOpEDT][flopedt] en créant un accès simple, personnalisé et portable à son emploi du temps.

Bien qu'il ait été conçu pour l'iut de Blagnac il reste facilement adaptable pour d'établissements.
Son disign est minimaliste pour faciliter la prise d'information sur la journée de l'utilisateur.

## Services

* 100% [Libre][libre]
* Affichage personalisé des cours
* Reprise des concepts de [FlOpEDT][flopedt] (citation, BK news)

## Apercu

![<img src="photo1.png" width=160>](https://framagit.org/flabarrere/flopedt_android/raw/master/asset/screenshot/main_18-12-2018_1.png)
![<img src="photo2.png" width=160>](https://framagit.org/flabarrere/flopedt_android/raw/master/asset/screenshot/connect_18-12-2018_1.png)

Retrouver plus de capture [ici](https://framagit.org/flabarrere/flopedt_android/tree/master/asset/screenshot)

Telechargez une implementation de FlOpEDT-Andoid [ici](https://play.google.com/store/apps/details?id=flopedt.flop_android)


## Utiliser FlOpEDT-Android

Utiliser FlOpEDT-Android ? Rien de plus simple, rendez vous [ici](https://framagit.org/flabarrere/flopedt_android/wikis/Documentation-Technique)

## Contribuer

Si vous souhaitez contribuer par des idées ou des services manquant vous êtes les bienvenus !

Soumettez votre [pull requests][pull-requests].

## Dependances

FlOpEDT_Android utilise:
* Un outil [FlOpEDT][flopedt].
* Une version [Android 4.0.3] Ice Cream Sandwich (Api 15) ou ultérieure


## License

[![GNU General Public License version 3](https://www.gnu.org/graphics/gplv3-127x51.png)](https://framagit.org/flabarrere/flopedt_android/blob/master/LICENSE)

    FlOpEDT_Android  Copyright (C) 2018 Da Costa Esteban, Labarrere Florian, Lamerly Steven

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

 [libre]: https://www.gnu.org/philosophy/free-sw.html
 [flopedt]: https://framagit.org/flopedt/FlOpEDT
 [pull-requests]: https://framagit.org/dystopia-project/simple-email/merge_requests
 [Android 4.0.3]: https://developer.android.com/about/versions/android-4.0.3