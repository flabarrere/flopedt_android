// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.data;


import java.io.Serializable;

/**
 * Cette classe permet de créer un groupe
 * 
 * @author Steven Lamerly
 * @version 1.1
 *
 */
public class Groupe implements Serializable, Comparable<Groupe>
{
    private String promo;
    private String nom;
    private Groupe parent;
    private boolean basic;
  
 /**
  * Construceur permettant de créer un groupe avec uniquement un nom
  * 
  * @param pfnom
  */
  public Groupe(String pfnom, String promo ){
      this.nom = pfnom;
      this.basic = true;
      this.promo = promo;

  }
  
  /**
   * Constructeur paramétré d'un groupe
   * 
   * @param pfnom
   * @param pfparent
   */
  public Groupe(String pfnom, Groupe pfparent){
      this( pfnom, pfparent.getPromo());
      this.parent = pfparent;
      pfparent.setBasic(false);

  }
  
  /**
   * Permet de récupérer le nom du groupe
   * 
   * @return String étant le nom du groupe
   */
  public String getnom(){
    return nom;
  }
  
  
  /**
   * Permet de récupérer l'ID d'un groupe
   * 
   * @return String étant l'ID du groupe
   */
  public String getID(){
    String ret;
    
    if (parent == null) {
      ret = nom;
    }
    else {
      ret = parent.getID() + "/" + nom;
    }
    return ret;
  }
    /**
     * Permet de savoir si le groupe est basic ( qu'il n'a pas d'enfant )
     *
     * @return true si il est basic
     */
    public boolean isBasic() {
        return basic;
    }

    /**
     * Permet de dire si le groupe est basic ( qu'il n'a pas d'enfant )
     *
     * @param basic true si il est basic
     */
    public void setBasic(boolean basic) {
        this.basic = basic;
    }
  
  /**
   * Permet de savoir si le groupe gr fais partie de l'arborescence d'un autre groupe
   * 
   * @param gr étant le groupe avec lequel on fait la comparaison
   * @return true si le groupe gr en fait bien partie, sinon false
   */
  public boolean is(Groupe gr){
	return gr.getPromo().equals(promo) && getID().startsWith(gr.getID());
  }

    @Override
    public String toString() {
        return getID();
    }

    public String getPromo() {
        return promo;
    }

    public void setPromo(String promo) {
        this.promo = promo;
    }

    @Override
    public int compareTo(Groupe o) {
        if (promo.equals(o.getPromo()))
            return getID().compareTo(o.getID());
        else
            return promo.compareTo(o.getPromo());
    }
}
