// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.data;

import android.support.annotation.NonNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class Semaine extends ArrayList<Jour> implements Comparable<Semaine>{



    public static DateFormat idFormat = new SimpleDateFormat("w-yyyy", Locale.FRENCH);
    private static DateFormat toStringFormat = new SimpleDateFormat("w", Locale.FRENCH);

    public String id() {
        if( this.isEmpty() )
            return "empty";
        else
            return idFormat.format(this.get(0).getDate());
    }

    @Override
    public int compareTo(@NonNull Semaine o) {
        return this.id().compareTo( o.id() );
    }

    @Override
    public boolean equals(Object o) {
        if( !(o instanceof Semaine))
            return false;
        return id().equals(((Semaine)o).id());
    }

    @Override
    public int hashCode() {
        return id().hashCode();
    }

    @Override
    public String toString() {
        return "Semaine "+ (this.isEmpty()? "0": toStringFormat.format( this.get(0).getDate() ));
    }
}
