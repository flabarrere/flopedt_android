// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.data;

import android.graphics.Color;

import java.io.Serializable;

/**
 * Cette classe permet de regrouper les �l�ments d'une breaking news
 * 
 * @author Steven Lamerly
 *
 */
public class BreakingNews implements Serializable {
	
	private String message;
	private String lien;
	private int couleurBG;
	private int couleurTxt;


    /**
	 * Constructeur sans lien de BreakingNews
     *
     * @param pfmessage message
	 */
	public BreakingNews(String pfmessage, String pfcolor_bg, String pfcolor_txt) {
		this( pfmessage, pfcolor_bg, pfcolor_txt, null );
	}
	
	/**
	 * Constructeur avec lien de BreakingNews
	 * 
	 * @param pfmessage
	 * @param pflien
	 */
	public BreakingNews(String pfmessage, String pfcolor_bg, String pfcolor_txt,  String pflien) {
		this.message = pfmessage;
		this.couleurBG = Color.parseColor(pfcolor_bg);
		this.couleurTxt = Color.parseColor(pfcolor_txt);
		this.lien = pflien;
	}
	
	/**
	 * Permet de r�cup�rer le message de la breaking news
	 * 
	 * @return String �tant le message de la breaking news
	 */
	public String getmessage() {
		return this.message;
	}
	
	/**
	 * Permet de r�cup�rer le lien de la breaking news
	 * 
	 * @return String �tant le lien de la breaking news
	 */
	public String getlien() {
		return this.lien;
	}
	
	/**
	 * Permet de modifier le message de la breaking news
	 * 
	 * @param pfmessage �tant le nouveau message de la breaking news
	 */
	public void setmessage(String pfmessage) {
		this.message = pfmessage;
	}
	
	/**
	 * Permet de modifier le lien de la breaking news
	 * 
	 * @param pflien �tant le nouveau lien de la breaking news
	 */
	public void setlien(String pflien) {
		this.lien = pflien;
	}

	/**
	 * Permet de r�cup�rer la couleur de la breaking news
	 *
	 * @return String �tant la couleur de la breaking news
	 */
	public int getCouleurBG() {
		return couleurBG;
	}

	/**
	 * Permet de r�cup�rer la couleur de texte de la breaking news
	 *
	 * @return String �tant la couleur de texte de la breaking news
	 */
	public int getCouleurTxt() {
		return couleurTxt;
	}

    /**
     * Permet de modifier le lien de la breaking news
     *
     * @return true si il y a un lien, sinon false
     */
	public boolean hasLien(){
	    return lien!=null;
    }

    @Override
    public String toString() {
        String str = message;
        if( lien!=null )
            str+= " -> " + lien;
        return str;
    }


}
