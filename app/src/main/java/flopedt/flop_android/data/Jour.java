// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.data;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * Cette classe permet de regrouper toute les informations d'un jour
 * 
 * @author Steven Lamerly
 *
 */
public class Jour implements Comparable<Jour>, Serializable {

	private static DateFormat dateFormat = new SimpleDateFormat("EEEE dd MMMM",Locale.FRENCH);

	private Date date;
	private Cours[] cours;
	private List<BreakingNews> bkNewsList;

	/**
	 * Constructeur par d�faut de Jour
	 */
	public Jour(Date pfDate, int nbCours ) {
	    this.date = pfDate;
		this.cours = new Cours[nbCours];
		this.bkNewsList = new ArrayList<>();
	}
	
	/**
	 * Permet de r�cup�rer le tableau de cours du jour
	 * 
	 * @return tableau de Cours
	 */
	public Cours[] getCoursArray() {
		return this.cours;
	}

	/**
	 * Permet de r�cup�rer le tableau de cours du jour
	 *
     * @param index index recherché
	 * @return le Cours a l'index
	 */
	public Cours getCours( int index ) {
		return this.cours[index];
	}

	/**
	 * Permet de r�cup�rer la liste breaking news du jour
	 * 
	 * @return la liste des breaking news
	 */
	public List<BreakingNews> getDBNewsList() {
		return bkNewsList;
	}
	
	/**
	 * Permet de modifier le tableau de cours du jour
	 * 
	 * @param pftabCours le nouveau tableau de Cours
	 */
	public void setCours(Cours pftabCours, int index ) {
		this.cours[index] = pftabCours;
	}

	/**
	 * Permet de r�cup�rer la date du jour
	 *
	 * @return la date du jour
	 */
	public String getDateText() {
		return dateFormat.format(date);
	}

	/**
	 * Permet de r�cup�rer la date du jour
	 *
	 * @return la date du jour
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Permet de modifier la date du jour
	 *
	 * @param date la nouvelle date du Cours
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	/**
	 * Permet de modifier la breaking news du jour
	 * @param pfbreakingnews la nouvelle breaking news
	 */
	public void addDBNews(BreakingNews pfbreakingnews) {
		this.bkNewsList.add(pfbreakingnews);
	}

	@Override
	public int compareTo(@NonNull Jour o) {
		if( o.equals(this))
			return 0;
		return -o.getDate().compareTo( this.date );
	}

    @Override
    public boolean equals(Object obj) {

	    if( ! (obj instanceof Jour) )
	        return false;
	    
        return dateFormat.format(((Jour) obj).getDate()).equals( dateFormat.format(date) );
    }

    @Override
    public int hashCode() {
        return dateFormat.format(date).hashCode();
    }

    @Override
    public String toString() {
	    StringBuilder sb  = new StringBuilder();

        sb.append( this.getDateText() ).append('\n');

	    for( Cours c : cours )
	        if( c!=null)
	            sb.append(c).append('\n');

	    for( BreakingNews b : bkNewsList )
	        sb.append(b).append('\n');

        return sb.toString();
    }

}
