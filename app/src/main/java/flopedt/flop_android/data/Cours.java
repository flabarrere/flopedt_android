// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.data;

import android.graphics.Color;

import java.io.Serializable;

/**
 * Cette classe permet de regrouper touts les éléments d'un cours
 *
 * @version 1.5
 * @author Steven Lamerly
 *
 */
public class Cours implements Serializable {
	
	private Professeur professeur;
	private Groupe groupe;
	private String module;
	private String salle;
	private int couleurBG;
	private int couleurTxt;


	
	/**
	 * Constructeur paramétré de Cours
	 *
	 * @param pfProfesseur
	 * @param pfGroup
	 * @param pfmodule
	 * @param pfroom
	 * @param pfcolor_bg
	 * @param pfcolor_txt
	 */
	public Cours(Professeur pfProfesseur, Groupe pfGroup, String pfmodule, String pfroom, String pfcolor_bg, String pfcolor_txt) {
		this.professeur = pfProfesseur;
		this.groupe=pfGroup;
		this.module = pfmodule;
		this.salle = pfroom;
		this.couleurBG = Color.parseColor(pfcolor_bg);
		this.couleurTxt = Color.parseColor(pfcolor_txt);
	}
	
	/**
	 * Permet de récupérer le professeur
	 * 
	 * @return Le professeur
	 */
	public Professeur getProfesseur() {
		return this.professeur;
	}
	
	/**
	 * Permet de récupérer le groupe
	 * 
	 * @return Le groupe
	 */
	public Groupe getGroupe() {
		return this.groupe;
	}

	
	/**
	 * Permet de récupérer l'abréviation du module
	 * 
	 * @return String étant l'abréviation du module
	 */
	public String getmodule() {
		return this.module;
	}

	
	/**
	 * Permet de récupérer la salle
	 * 
	 * @return String étant la salle
	 */
	public String getsalle() {
		return this.salle;
	}
	
	/**
	 * Permet de récupérer la couleur du background
	 * 
	 * @return String étant la couleur du background
	 */
	public int getcouleurBG() {
		return this.couleurBG;
	}
	
	/**
	 * Permet de récupérer la couleur du texte
	 * 
	 * @return String étant la couleur du texte
	 */
	public int getcouleurTxt() {
		return this.couleurTxt;
	}

	/**
	 * Permet de modifier le nom du professeur
	 * 
	 * @param pfProfesseur étant le nouveau nom du professeur
	 */
	public void setProfesseur(Professeur pfProfesseur) {
		this.professeur = pfProfesseur;
	}
	
	/**
	 * Permet de modifier le nom du groupe
	 * 
	 * @param pfGroupe étant le nouveau nom du groupe
	 */
	public void setGroupe(Groupe pfGroupe) {
		this.groupe = pfGroupe;
	}

	
	/**
	 * Permet de modifier l'abréviation du module
	 * 
	 * @param pfmodule étant la nouvelle abréviation du module
	 */
	public void setmodule(String pfmodule) {
		this.module = pfmodule;
	}
	
	/**
	 * Permet de modifier la salle
	 * 
	 * @param pfsalle étant la nouvelle salle
	 */
	public void setsalle(String pfsalle) {
		this.salle = pfsalle;
	}
	
	/**
	 * Permet de modifier la couleur du background
	 * 
	 * @param pfcolor_bg étant la nouvelle couleur du background
	 */
	public void setcouleurBG(int pfcolor_bg) {
		this.couleurBG = pfcolor_bg;
	}
	
	/**
	 * Permet de modifier la couleur du texte
	 * 
	 * @param pfcolor_txt étant la nouvelle couleur du texte
	 */
	public void setcouleurTxt(int pfcolor_txt) {
		this.couleurTxt = pfcolor_txt;
	}

	/**
	 * Permet de modifier la couleur du background
	 *
	 * @param pfcolor_bg étant la nouvelle couleur du background en html
	 */
	public void setcouleurBG(String pfcolor_bg) {
		this.couleurBG = Color.parseColor( pfcolor_bg ) ;
	}

	/**
	 * Permet de modifier la couleur du texte
	 *
	 * @param pfcolor_txt étant la nouvelle couleur du texte en html
	 */
	public void setcouleurTxt(String pfcolor_txt) {
		this.couleurTxt = Color.parseColor( pfcolor_txt ) ;
	}


    @Override
    public String toString() {
        String str= "";
        str += professeur.toString() + '-';
        str += groupe.toString()+ '-';
        str += module  + '-';
        str += salle ;
        return str;
    }

}
