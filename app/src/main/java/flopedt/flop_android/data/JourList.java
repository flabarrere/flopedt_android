// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.data;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.*;

public class JourList implements Iterable<Jour>{

    private List<Semaine> semaines;

    public JourList(){
        this.semaines = new ArrayList<>();
    }

    public Jour get( Date date ) {

        for( Jour j : this)
            if (j.getDate().equals( date ))
                return j;
        Jour newJour = new Jour( date, 6 );
        this.add( newJour );
        return newJour ;
    }

    public Jour get( int index ){
        Iterator<Jour> jourIterator = iterator();
        for( int i=0 ; i<index ; i++ )
            jourIterator.next();

        return jourIterator.next();
    }

    public Semaine getSemaine( int index )
    {
        return semaines.get( index );
    }

    public void add( Jour jour ){


        Semaine semaineOfDay = weekOfDay( jour );

        if( semaineOfDay ==null ) {
            semaineOfDay = new Semaine();
            semaines.add(semaineOfDay);
        }

        semaineOfDay.add( jour );
    }

    public void addAll( JourList jours ){
        if( jours==null )
            return;
        for( Jour j : jours)
            this.add(j);
    }

    public int getTodayIndex(){
        Jour today = new Jour( Calendar.getInstance().getTime(), 0);

        int index = 0;

        for( Jour j : this ){
            if( j.compareTo( today )>=0 )
                return  index;
            index++;
        }

        return 0;
    }

    public boolean contains( Date date ) {

        for (Jour j : this)
            if (j.getDate().equals(date))
                return true;
        return false;
    }

    public Semaine weekOfDay(Jour jour ){
        for( Semaine w : semaines)
            if( w.id().equals( Semaine.idFormat.format(jour.getDate()) ))
                return w ;
        return null;
    }

    public int size(){
        int size = 0 ;
        for( Semaine w : semaines)
            size += w.size() ;
        return size;
    }

    public boolean isEmpty(){
        return size()==0;
    }

    public void sort(){
        Collections.sort(semaines);
        for( Semaine w : semaines)
            Collections.sort(w);
    }

    @NonNull
    @Override
    public Iterator<Jour> iterator() {
        return new JourIterator();
    }

    private class JourIterator implements Iterator<Jour>{

        Iterator weekIterator ;
        Iterator dayIterator ;

        private boolean canDelete;

        private JourIterator(){
            this.weekIterator = semaines.iterator();
            if( weekIterator.hasNext() )
                this.dayIterator = ((Semaine)weekIterator.next()).iterator();
            this.canDelete = false ;
        }

        @Override
        public boolean hasNext() {
            return !isEmpty() && (weekIterator.hasNext() || dayIterator.hasNext());
        }

        @Override
        public Jour next() {
            if( !this.hasNext() )
                throw new NoSuchElementException();

            canDelete = true;

            while( !dayIterator.hasNext())
                dayIterator = ((Semaine)weekIterator.next()).iterator();

            return (Jour) dayIterator.next();
        }

        @Override
        public void remove() {
            if( !canDelete )
                throw new IllegalStateException();
        }
    }
}
