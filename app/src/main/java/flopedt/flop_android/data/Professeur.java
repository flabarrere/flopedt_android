// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.data;

import java.io.Serializable;

/**
 * Cette classe permet de regrouper les informations d'un professeur
 * 
 * @author Steven Lamerly
 * @version 2.0
 *
 */
public class Professeur  implements Serializable, Comparable<Professeur> {

	private String codeP;
	private String nomP;
	private String emailP;


	/**
	 * Constructeur param�tr� de Professeur
	 *
	 * @param pfcode_prof
	 * @param pfnom_prof
	 */
	public Professeur( String pfcode_prof, String pfnom_prof) {
		this( pfcode_prof, pfnom_prof, null );
	}

	/**
	 * Constructeur param�tr� de Professeur avec email
	 *
	 * @param pfcode_prof
	 * @param pfnom_prof
	 * @param pfemail_prof
	 */
	public Professeur( String pfcode_prof, String pfnom_prof, String pfemail_prof) {
		this.codeP = pfcode_prof;
		this.nomP = pfnom_prof;
		this.emailP = pfemail_prof;
	}
	
	/**
	 * Permet de r�cup�rer le nom du professeur
	 * 
	 * @return String �tant le nom du professeur
	 */
	public String getnomP() {
		return this.nomP;
	}
	
	/**
	 * Permet de r�cup�rer l'email du professeur
	 * 
	 * @return String �tant l'email du professeur
	 */
	public String getemailP() {
		return this.emailP;
	}
	
	/**
	 * Permet de modifier le nom du professeur
	 * 
	 * @param pfnom_prof �tant le nouveau nom du professeur
	 */
	public void setnomP(String pfnom_prof) {
		this.nomP = pfnom_prof;
	}
	
	/**
	 * Permet de modifier l'email du professeur
	 * 
	 * @param pfemail_prof �tant le nouvel email du professeur
	 */
	public void setemailP(String pfemail_prof) {
		this.emailP = pfemail_prof;
	}

	public boolean hasEmail(){
		return emailP!=null;
	}

	/**
	 * Permet de r�cup�rer l'identifiant du professeur
	 *
	 * @return �tant l'identifiant du professeur
	 */
	public String getCodeP() {
		return codeP;
	}

	/**
	 * Permet de modifier l'identifiant du professeur
	 *
	 * @param codeP �tant le nouvel identifiant du professeur
	 */
	public void setCodeP(String codeP) {
		this.codeP = codeP;
	}

	@Override
	public String toString() {
		return codeP + " " + nomP;
	}

	@Override
	public boolean equals(Object obj) {
		if( !( obj instanceof Professeur))
			return false;
		return ((Professeur) obj).getCodeP().equals( codeP );
	}

	@Override
	public int compareTo(Professeur o) {
		return this.nomP.compareToIgnoreCase(o.getnomP());
	}
}
