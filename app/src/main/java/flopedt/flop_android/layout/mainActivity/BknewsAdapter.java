// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.layout.mainActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import flopedt.flop_android.data.BreakingNews;

import java.util.List;

public class BknewsAdapter extends BaseAdapter {

    private List<BreakingNews> breakingNews;
    private Context context;

    public BknewsAdapter(Context context, List<BreakingNews> breakingNews ){

        this.context= context;
        this.breakingNews = breakingNews;


    }

    @Override
    public int getCount() {
        return breakingNews.size();
    }

    @Override
    public Object getItem(int position) {
        return breakingNews.get( position );
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return ViewItem.getBreakingNewsView(context, breakingNews.get(position)) ;
    }
}
