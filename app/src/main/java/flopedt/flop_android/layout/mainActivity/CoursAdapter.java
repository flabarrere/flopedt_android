// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.layout.mainActivity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import flopedt.flop_android.R;
import flopedt.flop_android.data.Cours;

import java.util.Arrays;

public class CoursAdapter extends BaseAdapter {

    private Cours[] coursArray;
//    private LayoutInflater inflater;
    private Context context;


    CoursAdapter(Context context, Cours[] cours ){
        this.coursArray = cours;
//        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return coursArray.length;
    }

    @Override
    public Object getItem(int position) {
        return coursArray[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return ViewItem.getCoursView(context, coursArray[position]);
    }
}
