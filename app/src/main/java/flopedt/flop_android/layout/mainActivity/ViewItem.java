// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.layout.mainActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;

import flopedt.flop_android.R;
import flopedt.flop_android.data.BreakingNews;
import flopedt.flop_android.data.Cours;

/**
 * @version 2.3
 * @author DA COSTA Esteban
 */
public class ViewItem {

    /**Renvoie une View correspondant au cours donné.
     *
     * @see CoursAdapter
     * @param pfContext le context de l'application
     * @param pfCours Le cours a mettre en view
     * @return View
     */
    public static View getCoursView (final Context pfContext, Cours pfCours){

        //regarde si le cours est null (pas la même view si le cas)

            //association de la ressource view
            View rView = View.inflate(pfContext, R.layout.cours_item, null);
            rView.setBackgroundResource(R.drawable.border);
        if (pfCours != null) {
            //modification des textes selon le cours ainsi que leurs coleurs
            if (!PreferenceManager.getDefaultSharedPreferences(pfContext).getBoolean("loginIsTutor", false))
                ((TextView) rView.findViewById(R.id.prof)).setText(pfCours.getProfesseur().getCodeP());
            else
                ((TextView) rView.findViewById(R.id.prof)).setText(pfCours.getGroupe().getPromo() + " - " + pfCours.getGroupe().getnom());
            ((TextView) rView.findViewById(R.id.prof)).setTextColor(pfCours.getcouleurTxt());
            ((TextView) rView.findViewById(R.id.module)).setText(pfCours.getmodule());
            ((TextView) rView.findViewById(R.id.module)).setTextColor(pfCours.getcouleurTxt());
            ((TextView) rView.findViewById(R.id.salle)).setText(pfCours.getsalle());

            ((TextView) rView.findViewById(R.id.salle)).setTextColor(pfCours.getcouleurTxt());

            //mise en place de la couleur du cours

            rView.getBackground().setColorFilter(pfCours.getcouleurBG(), PorterDuff.Mode.SRC);
        }
            return rView;
    }

    /**Renvoie une View correspondant à la breaking news donné.
     *
     * @see BknewsAdapter
     * @param context le context de l'application
     * @param breakingNews Le cours a mettre en view
     * @return View
     */
    public static View getBreakingNewsView (final Context context, final BreakingNews breakingNews){

        //Création de la BreakingNewsView et association de la view au XML
        View rView = View.inflate(context, R.layout.bn_item, null);

        //Modification
        ((TextView)rView.findViewById(R.id.nameBn)).setText(breakingNews.getmessage());
        ((TextView)rView.findViewById(R.id.nameBn)).setTextColor(breakingNews.getCouleurTxt());
        rView.setBackgroundColor(breakingNews.getCouleurBG());

        //Rends clickable la Breaking News
        rView.setClickable(true);


            //Définition de la fonction onClick en cas de lien
        if (breakingNews.hasLien())
            rView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(breakingNews.getlien()));
                    context.startActivity(browserIntent);
                }
            });

        return rView;
    }


}
