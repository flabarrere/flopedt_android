// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.layout.mainActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import flopedt.flop_android.R;
import flopedt.flop_android.data.JourList;
import flopedt.flop_android.django_interface.FileLoader;
import flopedt.flop_android.layout.connexionActivity.ConnexionActivity;

/**
 * @author LABARRERE Florian
 * @author DA COSTA Esteban
 * @version 1.8
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ((TextView)findViewById( R.id.quote)).setText( FileLoader.newInstance(this).getAQuote() );

        JourList jourList = FileLoader.getInstance().loadNextWeeks( 3 );

        SectionsPagerAdapter mSectionsPagerAdapter  = new SectionsPagerAdapter(getSupportFragmentManager(), jourList);

        ViewPager mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem( jourList.getTodayIndex() );

        //Lancement au démarrage si les préférences ne sont pas set
        if ( !PreferenceManager.getDefaultSharedPreferences(this).getBoolean("loginIsTutor", false) )
            if ( PreferenceManager.getDefaultSharedPreferences(this).getString("loginPromo", null) == null )
                startActivityForResult(new Intent(this, ConnexionActivity.class), 0);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        JourList jours;

        SectionsPagerAdapter(FragmentManager fm, JourList jours) {
            super(fm);
            this.jours = jours ;

        }

        @Override
        public Fragment getItem(int position) {
            return JourFragment.newInstance( jours.get(position) );
        }


        @Override
        public int getCount() {
            return jours.size();
        }

    }

    /**
     * Permet de connaitre les résultats de l'activité ConnexionActivity.
     * @param requestCode le code de la requête pour diférencier deux appels
     * @param resultCode le code de résultat qui définit la réponse de ConnexionActivity
     * @param data non utilisé
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        switch (requestCode){
            case 0 : // Cas ou les préférences ne sont pas initialisées (première ouverture)
                if (resultCode != RESULT_OK) { //On force l'utilisateur à mettre des préférences
                startActivityForResult(new Intent(this, ConnexionActivity.class), 0);

                }
                else { //On affiche un toast pour indiquer le chargement des cours
                    if(sp.getBoolean("loginIsTutor", false))
                        Toast.makeText(this, "Chargement du profil : " + sp.getString("loginProf", null), Toast.LENGTH_SHORT).show();
                    else {
                        Toast.makeText(this, "Chargement du profil : " + sp.getString("loginGroup", null), Toast.LENGTH_SHORT).show();
                    }

                    //Recréation de la MainActivity
                    finish();
                    startActivity(new Intent(this, MainActivity.class));

                }
                break;
            case 1 : // Cas où on modifie les préférences (appuie du bouton Pref)
                if (resultCode == RESULT_OK){ //On affiche un toast pour indiquer le chargement des cours
                    if(sp.getBoolean("loginIsTutor", false))
                        Toast.makeText(this, "Chargement du profil : " + sp.getString("loginProf", null), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(this, "Chargement du profil : " + sp.getString("loginGroup", null), Toast.LENGTH_SHORT).show();

                    //Recréation de la MainActivity
                    finish();
                    startActivity(new Intent(this, MainActivity.class));

                }
                break;
        }

    }

    /**
     * Action à effectuer au clic du bouton préference.
     * @param v la view du clic
     */
    public void onClickButtonPref (View v){
        startActivityForResult(new Intent(this, ConnexionActivity.class), 1);
    }

    /**
     * Action à effectuer au clic des quotes.
     * @param v la view du clic
     */
    public void onClickQuote (View v){
        Log.println(7, "onClickQuote", "Je rentre dans la méthode");
        ((TextView)findViewById( R.id.quote)).setText( FileLoader.getInstance().getAQuote() );
    }
}
