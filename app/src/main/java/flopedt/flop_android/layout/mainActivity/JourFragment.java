// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.layout.mainActivity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import flopedt.flop_android.R;
import flopedt.flop_android.data.Jour;

/**
 * A placeholder fragment containing a simple view.
 */
public class JourFragment extends Fragment {

    private Jour jour;

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_DAY = "day";

    public JourFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static JourFragment newInstance(Jour jour) {
        JourFragment fragment = new JourFragment();
        Bundle args = new Bundle();
        args.putSerializable( ARG_DAY, jour );
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.jour = (Jour) getArguments().getSerializable(ARG_DAY);
        View rootView = inflater.inflate(R.layout.fragment_jour, container, false);
        ((TextView)rootView.findViewById( R.id.date_textview)).setText( jour.getDateText() );
        ((ListView)rootView.findViewById( R.id.cours_list )).setAdapter( new CoursAdapter(getContext(), jour.getCoursArray()));
        ((ListView)rootView.findViewById( R.id.bknews_list )).setAdapter( new BknewsAdapter(getContext(), jour.getDBNewsList()));
        return rootView;
    }
}
