// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.layout.connexionActivity;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import flopedt.flop_android.data.Professeur;


/**
 * Adapteur du recyclerView
 * @author DA COSTA Esteban
 * @author LABARRERE Florian
 * @version 1.0
 */
public class ConnexionRecyclerAdapter extends RecyclerView.Adapter <ConnexionRecyclerAdapter.ConnexionRecyclerHolder> {

    private List<Professeur> professeurList;

    private String selectedTutorId;
    private Button selectedButton;

    /**
     * retourne l'ID du prof sélectionné.
     * @return l'ID du prof sélectionné
     */
    String getSelectedTutorId() {
        return selectedTutorId;
    }


    class ConnexionRecyclerHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private Button mButton;
        private String id;

        /**
         * Construit un holder avec un bouton lors de l'apparition à l'écran.
         * @param v le boutton du holder
         */
        ConnexionRecyclerHolder(Button v) {
            super(v);
            mButton = v;
            mButton.setOnClickListener(v1 -> {
                selectedTutorId = id;

                if (selectedButton != null) {
                    selectedButton.setClickable(true);
                    selectedButton.setTextColor(Color.BLACK);
                    selectedButton.setTypeface(Typeface.DEFAULT);
                }
                selectedButton = mButton;
                mButton.setTextColor(Color.parseColor("#228b22"));
                mButton.setTypeface(Typeface.DEFAULT_BOLD);
                mButton.setClickable(false);

            });
        }

        /**
         * Modifie le boutton du prof sélectionné.
         * @param newProfesseur le nouveau professeur sélectionné
         */
        void setProfesseur( Professeur newProfesseur ){
            this.id = newProfesseur.getCodeP();
            mButton.setText( newProfesseur.getnomP() );

            boolean selected;
            selected = id.equals(selectedTutorId);
            mButton.setClickable(!selected);
        }
    }

    /**
     * Construit un adapter contenant la liste des cours.
     * @param pfList
     */
    ConnexionRecyclerAdapter(ArrayList<Professeur> pfList) {
        professeurList = pfList;
    }

    /**
     * Créer un holder associé à un boutton.
     * Méthode appellée lorsque l'on srcoll et qu'il faut créer le holder
     * @param viewGroup la vue parente
     * @param i l'index de l'holder dans l'adapteur
     * @return le holder créé
     */
    @NonNull
    @Override
    public ConnexionRecyclerAdapter.ConnexionRecyclerHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Button v = new Button(viewGroup.getContext());
        v.setBackgroundColor(Color.TRANSPARENT);
        ConnexionRecyclerHolder ret = new ConnexionRecyclerHolder(v);
        return ret;
    }

    /**
     * Change le prof sélectionné.
     * @param holder le holder contenant le prof
     * @param i l'index de l'holder dans l'adapteur
     */
    @Override
    public void onBindViewHolder(@NonNull ConnexionRecyclerHolder holder, int i) {
        holder.setProfesseur( professeurList.get(i) );
    }

    /**
     * Retourne la taille de l'adapteur.
     * @return la taille de l'adapteur
     */
    @Override
    public int getItemCount() {
        return professeurList.size();
    }
}
