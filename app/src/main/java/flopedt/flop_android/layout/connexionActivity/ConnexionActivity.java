// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.layout.connexionActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import flopedt.flop_android.R;
import flopedt.flop_android.data.Groupe;
import flopedt.flop_android.data.Professeur;
import flopedt.flop_android.django_interface.FileLoader;

/**Activité de première connexion.
 * Activité ne se lançant que lors de la première ouverture de l'application pour syncroniser les préférences
 *
 * @version 2.0
 * @author DA COSTA Esteban
 */
public class ConnexionActivity extends AppCompatActivity{


    Spinner selectorPromo;
    ArrayAdapter adapterPromo;

    Spinner selectorGroup;
    ArrayAdapter adapterGroup;

    Map<String, List<String>> map;
    List<String> setPromo;

    RecyclerView recyclerViewTutor;
    RecyclerView.LayoutManager layoutManager;
    ConnexionRecyclerAdapter adapterTutor;

    boolean isTutor;

    SharedPreferences preferences;

    Button buttonStudent;
    Button buttonTutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connexion_activity);


        //Association des button à ceux du XML
        buttonStudent = findViewById(R.id.buttonStudent);
        buttonTutor = findViewById(R.id.buttonTutor);

        //Création de la HashMap contenant les promos et les groupes
        map = new HashMap<>();

        //Récupération des préférences utilisateurs
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        setPromo = new ArrayList<>();

        // Association des Spinner à ceux du XML
        selectorPromo = findViewById(R.id.selectorPromo);
        selectorGroup = findViewById(R.id.selectorGroup);

        //Association du recyclerView à celui du XML
        recyclerViewTutor = findViewById(R.id.recyclerViewConnexion);
        layoutManager = new LinearLayoutManager(this);
        recyclerViewTutor.setLayoutManager(layoutManager);

        //Change l'affichage en fonction du type d'utilisateur
        isTutor = preferences.getBoolean("loginIsTutor", false);
        this.changeVisibility();



        try {
            //Ajoute les groupes dans une Map
            FileLoader fL = FileLoader.getInstance();
            map.put("PROMO", null);
            for (Groupe g : fL.loadBasicGroup()) { //Boucle sur tout les groupes primaires
                if (!map.containsKey(g.getPromo()))
                    map.put(g.getPromo(), new ArrayList<String>());
                map.get(g.getPromo()).add(g.getnom()); // ajout du groupe à la promo correspondante
            }

            //tri du tableau de prof
            List<Professeur> professeurArrayList = new ArrayList<>(fL.loadTutor());
            Collections.sort(professeurArrayList);

            //Ajout de l'adapteur au recycler
            adapterTutor = new ConnexionRecyclerAdapter((ArrayList<Professeur>) professeurArrayList);
            recyclerViewTutor.setAdapter(adapterTutor);


            // Récupération des différentes promos dans une List
            setPromo.addAll(map.keySet());



            //Lien entre le Spinner et l'adapteur contenant les données (ici Promo)
            adapterPromo = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, setPromo);
            adapterPromo.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
            selectorPromo.setAdapter(adapterPromo);

            String promo = preferences.getString("loginPromo", null);
            if ( promo != null )
                selectorPromo.setSelection(getPromoInt(setPromo, promo));

            //Ajout d'un listener de Spinner au spinner
            selectorPromo.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (parent.getSelectedItem().equals("PROMO"))
                        selectorGroup.setAdapter(null);
                    else{
                        List<String> listGroup = map.get(parent.getSelectedItem()); Collections.sort(listGroup);
                        adapterGroup = new ArrayAdapter(getApplicationContext(), R.layout.support_simple_spinner_dropdown_item, listGroup);
                        adapterGroup.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                        selectorGroup.setAdapter(adapterGroup);

                        String group = preferences.getString("loginGroup", null);
                        if (group != null)
                            selectorGroup.setSelection(getPromoInt(listGroup, group));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Action au clic de la section Etudiant.
     * @param v la vue du clic
     */
    public void onClickStudent(View v){
        isTutor = false;
        this.changeVisibility();
        buttonStudent.setClickable(false);
        buttonTutor.setClickable(true);
    }

    /**
     * Action au clicl de la section Professeur.
     * @param v la vue du clic
     */
    public void onClickTutor(View v){
        isTutor = true;
        this.changeVisibility();
        buttonTutor.setClickable(false);
        buttonStudent.setClickable(true);

    }

    /**
     * Action au clic du bouton valider
     * @param v la vue du clic
     */
    public void onClickValidate (View v){

        SharedPreferences.Editor editor = preferences.edit();

        if (!isTutor) {
            if (selectorPromo.getSelectedItem() == "PROMO"){
                Toast.makeText(this, "Veuillez sélectionner une promo", Toast.LENGTH_SHORT).show();
                return;
            }else {

                editor.putBoolean("loginIsTutor", false);
                editor.putString("loginPromo", (String) selectorPromo.getSelectedItem());
                editor.putString("loginGroup", (String) selectorGroup.getSelectedItem());
//                editor.putString("loginProf", null);

            }
        }else {
            if (adapterTutor.getSelectedTutorId() == null) {
                Toast.makeText(this, "Veuillez sélectionner un Prof", Toast.LENGTH_SHORT).show();
                return;
            }else{
                editor.putBoolean("loginIsTutor", true);
//                editor.putString("loginPromo", null);
//                editor.putString("loginGroup", null);
                editor.putString("loginProf", adapterTutor.getSelectedTutorId());
            }

        }
        editor.apply();
        setResult(RESULT_OK);
        finish();

    }


    /**
     * Change la section sélectionné.
     * Change l'affichage par rapport à la section sélectionné
     */
    private void changeVisibility(){
        if (isTutor){
            buttonTutor.setText(Html.fromHtml("<U>Enseignant</U>"));
            buttonStudent.setText(Html.fromHtml("Etudiant"));

            selectorGroup.setVisibility(View.GONE);
            selectorPromo.setVisibility(View.GONE);

            recyclerViewTutor.setVisibility(View.VISIBLE);
        }
        else {
            buttonStudent.setText(Html.fromHtml("<U>Etudiant</U>"));
            buttonTutor.setText(Html.fromHtml("Enseignant"));
            selectorGroup.setVisibility(View.VISIBLE);
            selectorPromo.setVisibility(View.VISIBLE);

            recyclerViewTutor.setVisibility(View.GONE);
        }
    }

    /**
     * Permet de retourner l'indice d'un string dans une list.
     * @param list la list à parcourir
     * @param s le string à trouver
     * @return l'indice du string ou -1 si il n'est pas dans la list
     */
    private int getPromoInt (List<String> list, String s){
        for ( int i = 0 ; i<list.size(); i++ )
            if ( s.equals(list.get(i)) )
                return i;
        return -1;
    }
}
