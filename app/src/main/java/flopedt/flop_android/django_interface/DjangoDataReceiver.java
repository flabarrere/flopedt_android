// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.django_interface;

import android.content.Context;
import android.os.StrictMode;
import android.util.Log;
import flopedt.flop_android.R;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

import static android.content.Context.MODE_PRIVATE;

/**
 * DjangoDataReceiver permet de telecharger deferents fichiers et les stoquer en local dans l'application<br />
 *
 * Il faudra exploiter les fichiers plus tard a l'aide de {@link FileLoader}
 *
 * @author Florian Labarrere
 * @version 2.1
 *
 * @see FileLoader
 */
public class DjangoDataReceiver {


    private Context context;

    /**
     * Constructeur de DjangoDataReceiver
     *
     * @param context le contexte de l'application
     */
    public DjangoDataReceiver( Context context ){
        this.context = context ;

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

    }

    /**
     * Telecharge le fichiers semaine de cours a partir du lien defini dans les resources
     *
     * @param year annee de la semaine a telecharger
     * @param week semaine a telecharger
     * @return true si le fichier semaine a ete telechargee
     */
    public boolean downloadCourse( int year, int week  ){

        String subUrl = context.getResources().getString( R.string.course_url) ;
        String fileName = context.getResources().getString( R.string.course_filename) ;

        return  download( String.format( subUrl, year, week ), String.format( fileName , year, week ));
    }

    /**
     * Telecharge le fichier breakings news a partir du lien defini dans les resources
     *
     * @param year annee de la semaine a telecharger
     * @param week semaine a telecharger
     * @return true si le fichier breakings a ete telechargee
     */
    public boolean downLoadBkNews(  int year, int week  ){
        String subUrl = context.getResources().getString( R.string.bknew_url);
        String fileName = context.getResources().getString( R.string.bknew_filename );

        return download( String.format( subUrl, year, week ), String.format( fileName , year, week ) );
    }

    /**
     * Telecharge le fichier quotes a partir du lien defini dans les resources
     *
     * @return true si le fichier quotes a ete telechargee
     */
    public boolean downloadQuotes(){
        String subUrl = context.getResources().getString( R.string.quote_url );
        String fileName = context.getResources().getString( R.string.quote_filename);

        return download( subUrl, fileName );
    }

    /**
     * Telecharge le fichier groups a partir du lien defini dans les resources
     *
     * @return true si le fichier groups a ete telechargee
     */
    public boolean downloadGroups(){
        String subUrl = context.getResources().getString( R.string.group_url);
        String fileName = context.getResources().getString( R.string.group_filename );

        return download( subUrl, fileName );
    }

    /**
     * Telecharge le fichier tutors a partir du lien defini dans les resources
     *
     * @return true si le fichier tutors a ete telechargee
     */
    public boolean downloadTutors(){
        String subUrl = context.getResources().getString( R.string.tutor_url );
        String fileName = context.getResources().getString( R.string.tutor_filename);

        return download( subUrl, fileName );
    }

    /**
     * Telecharge le fichier version a partir du lien defini dans les resources
     *
     * @return true si le fichier version a ete telechargee
     */
    public boolean downloadVersions(){
        String subUrl = context.getResources().getString( R.string.version_url );
        String fileName = context.getResources().getString( R.string.django_version_filename);

        return download( subUrl, fileName );
    }

    /**
     * Telecharge un fichier par http
     * @param sourceUrl le lien du fichier a telecharger
     * @param target le fichier d'arrivee
     * @return true si tout c'est bien passe, false sinon
     */
    private boolean download( String sourceUrl, String target){
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        String serverIP = context.getResources().getString(R.string.edturl);
        Log.i( "DjangoDataReceiver", "Download from :"  + serverIP + sourceUrl+" to "+target);
        try {
            URL url = new URL(serverIP + sourceUrl );
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                Log.e("DjangoDataReceiver","Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage());
                return false;
            }

            // download the file
            input = connection.getInputStream();
            output = context.openFileOutput( target, MODE_PRIVATE );

            byte data[] = new byte[4096];
            int count;
            while ((count = input.read(data)) != -1) {
                // allow canceling with back button
                output.write(data, 0, count);
            }

            Log.i( "DjangoDataReceiver", "Download done");

        } catch (Exception e) {
            Log.e("DjangoDataReceiver", e.toString() );
            return false;
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {}

            if (connection != null)
                connection.disconnect();
        }
        return true;
    }


}
