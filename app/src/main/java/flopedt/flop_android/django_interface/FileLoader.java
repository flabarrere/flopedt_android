// This file is part of the FlOpEDT_Android project.
// Copyright (c) 2018
// Authors: Da Costa Esteban, Labarrere Florian, Lamerly Steven
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with this program. If not, see
// <http://www.gnu.org/licenses/>.

package flopedt.flop_android.django_interface;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import flopedt.flop_android.R;
import flopedt.flop_android.data.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.*;

/**
 * FileLoader permet de recupere des données a partir de fichiers stoques en local<br />
 *
 * Ces fichiers peuvent etre telecharger a l'aide de {@link DjangoDataReceiver}
 *
 * @author Florian Labarrere
 * @version 2.2
 * @see DjangoDataReceiver
 */
public class FileLoader {

    private static FileLoader instance = null;


    private static JSONParser parser = new JSONParser();

    private Context context;
    private Calendar dateCursor;
    private DjangoDataReceiver downloader;

    private Map<String,Long> localVersionsMap;
    private Map<String,Long> djangoVersionsMap;
    private Set<Professeur> professeurs;
    private Set<Groupe> groupes;

    /**
     * Constructeur de FileLoader
     *
     * @param context Contexte de l'application
     */
    private FileLoader(Context context){
        this.context = context ;
        this.dateCursor = Calendar.getInstance();
        this.downloader = new DjangoDataReceiver( context );
        if( !downloader.downloadVersions())
            Log.e("FileLoader", "Pas de fichier version");
        try {
            localVersionsMap = loadVersionFile( context.getResources().getString(R.string.local_version_filename));
        } catch (Exception e) {
            Log.w("FileLoader", e.toString());
            localVersionsMap = new HashMap<>();
        }

        try {
            djangoVersionsMap = loadVersionFile( context.getResources().getString(R.string.django_version_filename));
        } catch (Exception e) {
            Log.w("FileLoader", e.toString());
            djangoVersionsMap = new HashMap<>();
        }

    }

    public static FileLoader newInstance( Context context ){
        instance = new FileLoader( context );
        return instance;
    }

    public static FileLoader getInstance(){
        return instance;
    }


    private Map<String,Long> loadVersionFile(String filename ) throws IOException, ParseException {

        Map<String,Long> versionMap = new HashMap<>();

        FileInputStream fis= context.openFileInput( filename );

        Iterator iterator = ((JSONArray) parser.parse( new InputStreamReader(fis) )).iterator();
        JSONObject jsonObject;
        Calendar calendar = Calendar.getInstance();
        String key;

        while( iterator.hasNext() ) {
            jsonObject = (JSONObject) iterator.next();
            int an   = (int) ((long) jsonObject.get("an"));
            int week = (int) ((long) jsonObject.get("semaine"));
            calendar.set( Calendar.YEAR,         an );
            calendar.set( Calendar.WEEK_OF_YEAR, week );

            key = Semaine.idFormat.format(calendar.getTime());

            versionMap.put( key, (long) jsonObject.get("version") );
        }
        return versionMap;
    }

    public void saveVersionsFile(){

        JSONArray list = new JSONArray();
        JSONObject object ;
        String date[];

        for( Map.Entry<String,Long> entry : localVersionsMap.entrySet() ){

            object = new JSONObject();
            date = entry.getKey().split("-");

            object.put("semaine", Long.valueOf(date[0]));
            object.put("an",      Long.valueOf(date[1]));
            object.put("version", entry.getValue());


            list.add( object );
        }

        try{
            String fileName = context.getResources().getString( R.string.local_version_filename);
            FileOutputStream fis = context.openFileOutput( fileName, Context.MODE_PRIVATE );
            Writer file = new OutputStreamWriter( fis );
            file.write(list.toJSONString());
            file.flush();
            file.close();
            fis.close();

        } catch (IOException ignored) {}
    }

    /**
     * Charge un dictionaire de {@link Jour} des semaines suivantes comptenant {@link Cours} et {@link BreakingNews} respectifs
     *
     * @param count le nombre de semaine a charger
     * @return Les jours des semaines suivantes
     */
    public JourList loadNextWeeks( int count ){

        JourList jours = new JourList();

        for( int i=0 ; i<count ; i++ )
            jours.addAll(loadNextWeek());

        jours.sort();
        return jours;
    }

    /**
     * Charge un dictionaire de {@link Jour} de la semaine suivante comptenant {@link Cours} et {@link BreakingNews} respectifs
     *
     * @return Les jours de la semaine suivante
     */
    public JourList loadNextWeek(){

        JourList jours;
        int year, week;
        year = dateCursor.get( Calendar.YEAR );
        week = dateCursor.get( Calendar.WEEK_OF_YEAR );

//        while ( jours.isEmpty() && djangoVersionsMap.containsKey( week+"-"+year) ){
            jours = loadWeek( year, week );

            dateCursor.add( Calendar.WEEK_OF_YEAR, 1 );
//            year = dateCursor.get( Calendar.YEAR );
//            week = dateCursor.get( Calendar.WEEK_OF_YEAR );
//        }
        return jours;
    }

    /**
     * Charge un dictionaire de {@link Jour} de la semaine comptenant {@link Cours} et {@link BreakingNews} respectifs
     *
     * @param year L'annee de la semaine demandée
     * @param week le numero de la semaine dans l'annee demandés
     * @return Les jours de la semaine demandés
     */
    public JourList loadWeek( int year, int week ){


        JourList jours = new JourList();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        boolean loginIsTutor = preferences.getBoolean( "loginIsTutor", false);
        String loginPromo = preferences.getString( "loginPromo", null);
        String loginGroup = preferences.getString( "loginGroup", null );
        String loginProf  = preferences.getString( "loginProf", null );

        String coursFileName = context.getResources().getString( R.string.course_filename, year, week );
        String bkNewFileName = context.getResources().getString( R.string.bknew_filename, year, week );

        String key = week + "-" + year;
        if( djangoVersionsMap.containsKey(key) &&
                ((!localVersionsMap.containsKey(key))
                || !(localVersionsMap.get(key).equals( djangoVersionsMap.get(key))))){

            downloader.downloadCourse( year, week );
            downloader.downLoadBkNews( year, week );

            localVersionsMap.put(key, djangoVersionsMap.get(key));
        }

        FileInputStream fisCours, fisBkNews;

        try{
            fisCours = context.openFileInput( coursFileName );
        } catch (FileNotFoundException e) {
            downloader.downloadCourse( year, week );
            try {
                fisCours = context.openFileInput( coursFileName );
            } catch (FileNotFoundException e1) {
                return null;
            }
        }

        try{
            fisBkNews = context.openFileInput( bkNewFileName );
        } catch (FileNotFoundException e) {
            downloader.downLoadBkNews( year, week );
            try {
                fisBkNews = context.openFileInput( bkNewFileName );
            } catch (FileNotFoundException e1) {
                return null;
            }
        }

        Calendar calendar = Calendar.getInstance();
        calendar.set( Calendar.YEAR         , year );
        calendar.set( Calendar.WEEK_OF_YEAR , week );

        BufferedReader br;
        String line;
        String split[];

        try {
            br = new BufferedReader( new InputStreamReader( fisCours ));

            if( !br.readLine().equals("id_cours,num_cours,prof_nom,gpe_nom,gpe_promo,module,jour,heure,room,room_type,color_bg,color_txt")) {

                Log.e( "FileLoader", "le fichier " + coursFileName + " semble avoir un probleme");
                context.deleteFile( coursFileName );

            }
            else{

                Cours cours;
                line = br.readLine();
                while ( line!=null ){
                    split = line.split(",");
                    line = br.readLine();
                    calendar.set( Calendar.DAY_OF_WEEK, Integer.parseInt(split[6])+2);

                    cours = new Cours( getTutorByCode(split[2]), getGroupByName(split[4], split[3]), split[5], split[8], split[10], split[11] );


                    if( loginIsTutor? getTutorByCode( loginProf ).equals( cours.getProfesseur()):
                            getGroupByName( loginPromo,loginGroup).is( cours.getGroupe() ) )
                        jours.get(calendar.getTime()).setCours(cours, Integer.parseInt(split[7]));

                }
            }

            // chargement des bknews

            br = new BufferedReader( new InputStreamReader( fisBkNews ));

            if( !br.readLine().equals("id,x_beg,x_end,y,txt,is_linked,fill_color,strk_color")) {

                Log.e( "FileLoader", "le fichier " + coursFileName + " semble avoir un probleme");
                context.deleteFile( bkNewFileName );

            }
            else{


                BreakingNews breakingNews;
                line = br.readLine();
                while ( line!=null ){
                    split = line.split(",");
                    line = br.readLine();

                    if( split[5].isEmpty())
                        breakingNews = new BreakingNews( split[4], split[6], split[7] );
                    else
                        breakingNews = new BreakingNews( split[4], split[6], split[7], split[5] );

                    for( int i=(int)Double.parseDouble(split[1]) ; i<(int)Double.parseDouble(split[2]) ; i++){
                        calendar.set( Calendar.DAY_OF_WEEK, i+2 );
                        if(jours.contains( calendar.getTime()))
                            jours.get(calendar.getTime()).addDBNews( breakingNews );
                    }

                }
            }


        } catch (Exception ignored) {}


        saveVersionsFile();
        jours.sort();
        return jours;
    }

    public Set<Professeur> loadTutor() throws Exception {

        if( professeurs!=null )
            return professeurs;

        professeurs = new HashSet<>();

        String filename = context.getResources().getString( R.string.tutor_filename );

        FileInputStream fis;
        BufferedReader br;

        try {
            fis = context.openFileInput( filename );
        } catch (FileNotFoundException e) {
            downloader.downloadTutors();
            fis = context.openFileInput( filename );
        }

        br = new BufferedReader( new InputStreamReader( fis ));

        String line = br.readLine();
        String split[];

        while( line!=null ){

            split = line.split(",");

            professeurs.add( new Professeur( split[0], split[2] + " " + split[1]
                                , split[3].isEmpty()? null : split[3]));
            
            line = br.readLine();
        }

        return professeurs;

    }

    public Professeur getTutorByCode( String code ){

        try {

            for( Professeur p : loadTutor() )
                if( p.getCodeP().equals(code) )
                    return p;

        } catch (Exception ignored) {}

        downloader.downloadTutors();
        professeurs = null;

        try {

            for( Professeur p : loadTutor() )
                if( p.getCodeP().equals(code) )
                    return p;

        } catch (Exception ignored) {}

        return null;

    }

    public Set<Groupe> loadBasicGroup(){
        Set<Groupe> basicGroupes = new HashSet<>();

        try {

            for (Groupe g : loadGroup() )
                if (g.isBasic())
                    basicGroupes.add(g);

        }catch ( Exception ignored){}

        return basicGroupes;
    }

    public Set<Groupe> loadGroup() throws Exception {

        if( groupes != null)
            return groupes;

        groupes = new HashSet<>();

        String filename = context.getResources().getString( R.string.group_filename );

        FileInputStream fis;

        try {
            fis = context.openFileInput( filename );

        } catch (FileNotFoundException e) {
            downloader.downloadGroups();
            fis = context.openFileInput( filename );
        }

        Iterator iterator = ((JSONArray) parser.parse( new InputStreamReader(fis) )).iterator();
        JSONObject jsonGroup;
        Groupe groupe;

        while( iterator.hasNext() ) {
            jsonGroup = (JSONObject) iterator.next();
            groupe = new Groupe((String)jsonGroup.get("name"), (String) jsonGroup.get("promo"));
            groupes.add( groupe );
            groupes.addAll( loadSubgroup( groupe, (JSONArray) jsonGroup.get("children")) );
        }

        return groupes;
    }

    private Set<Groupe> loadSubgroup( Groupe group, JSONArray subGroupArray ){


        Set<Groupe> subgroupes = new HashSet<>();

        if( subGroupArray==null )
            return subgroupes;

        Iterator iterator = subGroupArray.iterator();
        JSONObject jsonGroup;
        Groupe groupe;

        while( iterator.hasNext() ) {
            jsonGroup = (JSONObject) iterator.next();
            groupe = new Groupe((String) jsonGroup.get("name"), group);
            subgroupes.add( groupe );
            subgroupes.addAll( loadSubgroup( groupe, (JSONArray) jsonGroup.get("children")) );
        }

        return subgroupes;
    }

    public Groupe getGroupByName( String promo, String groupName ){
        try {
            for( Groupe g : loadGroup() )
                if( g.getPromo().equals( promo) && g.getnom().equals( groupName ) )
                    return g;

        } catch (Exception ignored) {}

        return null;
    }

    public String getAQuote(){

        FileInputStream fis;

        try {
            fis = context.openFileInput( context.getResources().getString( R.string.quote_filename ));
        } catch (FileNotFoundException e) {
            downloader.downloadQuotes();
            try {
                fis = context.openFileInput( context.getResources().getString( R.string.quote_filename ));
            } catch (FileNotFoundException e1) {
                e.printStackTrace();
                return "";
            }
        }
        try {

            JSONArray jsonArray = (JSONArray)parser.parse( new InputStreamReader(fis) ) ;
            Iterator iterator = jsonArray.iterator();

            int randomIndex = (int) (Math.random() * jsonArray.size());
            for( int i=0 ; i< randomIndex ; i++ )
                iterator.next();
            return (String) ((JSONObject)iterator.next()).get("txt");

        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

}
